# burr-lang

the good parts of smalltalk

- simple syntax with light punctuation
- metaprogramming
- using the CLOS
- with a emphasis on readability

inspired by:

- smalltalk
- dylan, common lisp
- logo
- wren
- red, rebol
- lua

## examples:

``````````````
to: n .UInt .factorial .UInt :do{
     `could be to: factorial( n ) :do:..., the .UInt is unnecessary`
  reduce_( range( 0 n ) )_with_(
     `underscores are always ignored outside of strings`
    : a b :{ a + 1 * b } `no operator precedence, (a + 1) * b is the same`
  )_starting_at_( 1 )
}

to: n .rec_factorial :do:
  if( n < 1 )then{ 1 }else{ n * (n - 1) .rec_factorial }
}

to: variables .demonstrate :do{
  let: counter := 0
  const: limit := 100
  const: rate  := 1
  while: counter < limit :do{
    trace!( counter )
    ` a good naming scheme is:
      - end with ! if it has side effects
      - end it with ? if it's a predicate
      - don't capitalize unless it's global or it's a class
      - no camel case, use snake_case or kebab-case. Since underscores are
        ignored, snake_case is the same as nocase, and nocase is fine if
        necessary
      - begin with -> for conversions, e.g. num .->string `
    counter <- (counter + 1) `<- is assignment`
  }
}
  

```syntactic elements```

`comment`
let: literals := [ `list of`
  123_456_789 `number, underscores are always ignored`
  name 'name with @bsolut3ly anything inside'
  `technically those are symbols, which may or may not represent a variable`
  "string"  "string with ""double quotes"""
  ` strings don't use normal escapes, so that the language will be more
    customizable, e.g. a regex library could use \ to escape without needing
    to escape the backslashes: "[^ ""'\\[\]()\n\t]*\.html?" instead of
   "[^ \"'\\\\[\\]()\n\t]*\\.html?" `
] `notice it doesn't need commas, but the newlines aren't significant!`

let: operators := [
  subject .postfix
  subject .postfix_with_$YMb0ls
  subj0 .infix. subj1
  ` postfix has higher precedence/binding power, so ``a .b .c. d .e .f. g .h``
    is ``((a .b) .c. (d .e)) .f. (g .h)``. It's the way you think, not the way
    you write math `

  subj0 + subj1
  ` it's automagically infix if it starts with a symbol, unless it's annotated
    with .s and :s `

  mixfix( subj0 )can( subj1 )have( many )arguments( subj3 )( subj4 )
  ` arguments in parentheses create normal value(s)`

  ( parentheses are actually calling the function/method with the name of
    '' with these arguments )

  reduce[ array arguments ]with( normal areguments )starting_at(normal)
  ` [] collects its insides in an array/list and then passes that like
    a normal () value `

  [ this is calling the '' function with an array/list ]

  do: stuff :end
  ` using colons like brackets will capture its arguments without evaluating
    them. `

  : arguments :{ block body }
  ` using {} will create a thunk that may be easier to manipulate than the
    ast returned by colon: :arguments, so normally use them instead when
    defining control structures `
  a .mixture: of :mixfix{ and }post/infix. arg
]

` that's all the syntax, everything else is semantics `

``````````````


